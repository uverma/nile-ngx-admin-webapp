import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicSwitcherComponent } from './topic-switcher.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ThemeModule } from '../../@theme/theme.module';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { TopicEventService } from '../../services/events/topicEvent.service';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TopicService } from '../../services/topic.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';

describe('TopicSwitcherComponent', () => {
  let component: TopicSwitcherComponent;
  let fixture: ComponentFixture<TopicSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ThemeModule,
        RouterTestingModule,
        FormsModule,
        NgSelectModule,
        ToasterModule,
      ],
      declarations: [TopicSwitcherComponent],
      providers: [
        HttpClient,
        HttpHandler,
        ClusterEventService,
        TopicEventService,
        TopicService,
        ToasterService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
