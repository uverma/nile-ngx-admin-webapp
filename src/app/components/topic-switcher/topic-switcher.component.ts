import { Component, OnInit } from '@angular/core';
import { TopicService } from '../../services/topic.service';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { TopicEventService } from '../../services/events/topicEvent.service';
import 'style-loader!angular2-toaster/toaster.css';
import Notification, {
  INotificationType,
} from '../notifications/notifications.component';
import { ToasterConfig, ToasterService } from 'angular2-toaster';

@Component({
  selector: 'cern-topic-switcher',
  templateUrl: './topic-switcher.component.html',
  styleUrls: ['./topic-switcher.component.scss'],
})
export class TopicSwitcherComponent implements OnInit {
  topics: any[] = [];
  selectedTopic: any;
  toasterConfig: ToasterConfig;

  constructor(
    private topicService: TopicService,
    private clusterEventService: ClusterEventService,
    private topicEventService: TopicEventService,
    private toasterService: ToasterService,
  ) {
    this.clusterEventService.events.subscribe(() => {
      this.selectedTopic = null;
      this.topics = null;
      this.queryTopics();
    });
  }

  ngOnInit() {
    this.queryTopics();
  }

  queryTopics() {
    if (
      this.clusterEventService.selectedCluster !== null &&
      this.clusterEventService.selectedCluster !== ''
    ) {
      this.topicService
        .getTopics(this.clusterEventService.selectedCluster)
        .subscribe(
          data => (this.topics = data),
          err => {
            const notification = new Notification({
              type: INotificationType.error,
              message: err.error.message,
            });
            this.toasterConfig = notification.config;
            this.toasterService.popAsync(notification.toaster);
            console.log(err);
          },
        );
    }
  }

  onTopicChange() {
    this.topicEventService.dispatchEvent(this.selectedTopic);
  }
}
