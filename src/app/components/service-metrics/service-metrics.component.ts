import { Component, OnInit } from '@angular/core';
import { ClusterEventService } from '../../services/events/clusterEvent.service';

@Component({
  selector: 'cern-service-metrics',
  templateUrl: './service-metrics.component.html',
  styleUrls: ['./service-metrics.component.scss'],
})
export class ServiceMetricsComponent implements OnInit {
  instances: number = 6;
  maxInstances: number = 10;
  vcpus: number = 200;
  maxVcpus: number = 400;
  memory: number = 100;
  maxMemory: number = 250;
  volumes: number = 250;
  maxVolumes: number = 300;

  grafana =
    'https://monit-grafana.cern.ch/d/000000290/kafka-cluster?refresh=1m&orgId=9&theme=light';

  selectedCluster: string;

  constructor(private clusterEventService: ClusterEventService) {
    this.clusterEventService.events.subscribe(() => {
      this.selectedCluster = this.clusterEventService.selectedCluster;
      console.log(this.selectedCluster);
    });
  }

  ngOnInit() {}
}
