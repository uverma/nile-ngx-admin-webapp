import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceMetricsComponent } from './service-metrics.component';
import { ThemeModule } from '../../@theme/theme.module';
import { RouterTestingModule } from '@angular/router/testing';
import { SafePipe } from '../../@theme/pipes/safe.pipe';
import { ClusterEventService } from '../../services/events/clusterEvent.service';

describe('ServiceMetricsComponent', () => {
  let component: ServiceMetricsComponent;
  let fixture: ComponentFixture<ServiceMetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThemeModule, RouterTestingModule],
      declarations: [ServiceMetricsComponent, SafePipe],
      providers: [ClusterEventService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
