import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicInfoComponent } from './topic-info.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ThemeModule } from '../../@theme/theme.module';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { KeyValuePipe } from '../../@theme/pipes/kv.pipe';

describe('TopicInfoComponent', () => {
  let component: TopicInfoComponent;
  let fixture: ComponentFixture<TopicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThemeModule, RouterTestingModule],
      declarations: [TopicInfoComponent, KeyValuePipe],
      providers: [ClusterEventService, NgbModalStack],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
