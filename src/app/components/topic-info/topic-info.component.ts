import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TopicEditModalComponent } from '../topic-edit-modal/topic-edit-modal.component';

@Component({
  selector: 'cern-topic-info',
  templateUrl: './topic-info.component.html',
  styleUrls: ['./topic-info.component.scss'],
})
export class TopicInfoComponent implements OnInit {
  @Input('data') topicData: any = {};

  constructor(private modalService: NgbModal) {}

  ngOnInit() {}

  editTopic() {
    const activeModal = this.modalService.open(TopicEditModalComponent, {
      size: 'sm',
      backdrop: 'static',
      container: 'nb-layout',
    });
    activeModal.componentInstance.topicData = this.topicData;
  }
}
