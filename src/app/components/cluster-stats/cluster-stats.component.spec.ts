import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterStatsComponent } from './cluster-stats.component';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { RouterTestingModule } from '@angular/router/testing';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ClusterService } from '../../services/cluster.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';

describe('ClusterStatsComponent', () => {
  let component: ClusterStatsComponent;
  let fixture: ComponentFixture<ClusterStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ThemeModule,
        Ng2SmartTableModule,
        RouterTestingModule,
        ToasterModule,
      ],
      declarations: [ClusterStatsComponent],
      providers: [
        ClusterEventService,
        HttpClient,
        HttpHandler,
        ClusterService,
        ToasterService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
