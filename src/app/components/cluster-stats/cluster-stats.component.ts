import { Component, OnInit } from '@angular/core';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { ClusterService } from '../../services/cluster.service';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import Notification, {
  INotificationType,
} from '../notifications/notifications.component';

import 'style-loader!angular2-toaster/toaster.css';

@Component({
  selector: 'cern-cluster-stats',
  templateUrl: './cluster-stats.component.html',
  styleUrls: ['./cluster-stats.component.scss'],
})
export class ClusterStatsComponent implements OnInit {
  nodes: number;
  controller: string;
  clusterInfoData: any = {};
  toasterConfig: ToasterConfig;

  constructor(
    private clusterEventService: ClusterEventService,
    private clusterService: ClusterService,
    private toasterService: ToasterService,
  ) {
    this.clusterEventService.events.subscribe(() => {
      // console.log(this.clusterEventService.selectedCluster);
      this.queryClusterInfo();
    });
  }

  ngOnInit() {
    this.queryClusterInfo();
  }

  queryClusterInfo() {
    if (
      this.clusterEventService.selectedCluster !== undefined &&
      this.clusterEventService.selectedCluster !== ''
    ) {
      this.clusterService
        .getClusterInfo(this.clusterEventService.selectedCluster)
        .subscribe(
          data => (this.clusterInfoData = data),
          err => {
            const notification = new Notification({
              type: INotificationType.error,
              message: err.error.message,
            });
            this.toasterConfig = notification.config;
            this.toasterService.popAsync(notification.toaster);
            console.error(err);
          },
        );
    }
  }
}
