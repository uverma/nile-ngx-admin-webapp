import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterInfoComponent } from './cluster-info.component';
import { ThemeModule } from '../../@theme/theme.module';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ObjectToObjectArrayPipe } from '../../@theme/pipes/objToArrObjs.pipe';

describe('ClusterInfoComponent', () => {
  let component: ClusterInfoComponent;
  let fixture: ComponentFixture<ClusterInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThemeModule],
      declarations: [ClusterInfoComponent, ObjectToObjectArrayPipe],
      providers: [ClusterEventService, HttpClient, HttpHandler],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
