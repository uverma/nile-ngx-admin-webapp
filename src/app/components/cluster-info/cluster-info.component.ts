import { Component, OnInit } from '@angular/core';
import { ClusterEventService } from '../../services/events/clusterEvent.service';

@Component({
  selector: 'cern-cluster-info',
  templateUrl: './cluster-info.component.html',
  styleUrls: ['./cluster-info.component.scss'],
})
export class ClusterInfoComponent implements OnInit {
  clusterInfo = [];

  constructor(private clusterEventService: ClusterEventService) {
    this.clusterEventService.events.subscribe(() => {
      console.log(this.clusterEventService.selectedCluster);
      this.clusterInfo = this.clusterEventService.selectedClusterData;
    });
  }

  ngOnInit() {
    if (this.clusterEventService.selectedClusterData !== undefined) {
      this.clusterInfo = this.clusterEventService.selectedClusterData;
    }
  }
}
