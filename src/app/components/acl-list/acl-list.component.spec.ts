import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AclListComponent } from './acl-list.component';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { AclService } from '../../services/acl.service';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { TopicEventService } from '../../services/events/topicEvent.service';
import { FlatPropsAclPipe } from '../../@theme/pipes/flatProps.pipe';
import { ToasterModule, ToasterService } from 'angular2-toaster';

describe('AclListComponent', () => {
  let component: AclListComponent;
  let fixture: ComponentFixture<AclListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ThemeModule,
        Ng2SmartTableModule,
        RouterTestingModule,
        ToasterModule,
      ],
      declarations: [AclListComponent, FlatPropsAclPipe],
      providers: [
        HttpClient,
        HttpHandler,
        AclService,
        ClusterEventService,
        TopicEventService,
        ToasterService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AclListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
