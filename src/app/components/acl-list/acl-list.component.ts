import { Component, OnInit, Input } from '@angular/core';
import { AclService } from '../../services/acl.service';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { TopicEventService } from '../../services/events/topicEvent.service';
import 'style-loader!angular2-toaster/toaster.css';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import Notification, {
  INotificationType,
} from '../notifications/notifications.component';
import { AclBinding, ResourceType } from '../../models/acl.model';

@Component({
  selector: 'cern-acl-list',
  templateUrl: './acl-list.component.html',
  styleUrls: ['./acl-list.component.scss'],
})
export class AclListComponent implements OnInit {
  data: any[];
  settings: {};
  showAcls = true;
  toasterConfig: ToasterConfig;

  @Input('selectedTopic') selectedTopic: string;

  constructor(
    private aclService: AclService,
    private clusterEventService: ClusterEventService,
    private topicEventService: TopicEventService,
    private toasterService: ToasterService,
  ) {
    this.settings = this.aclService.aclSettings();
    this.clusterEventService.events.subscribe(() => {
      this.data = null;
    });
    this.topicEventService.events.subscribe(() => {
      this.queryAcls();
    });
  }

  ngOnInit() {}

  queryAcls() {
    if (
      this.clusterEventService.selectedCluster !== undefined &&
      this.clusterEventService.selectedCluster !== '' &&
      this.topicEventService.selectedTopic !== undefined &&
      this.topicEventService.selectedTopic !== ''
    ) {
      this.aclService
        .getAcls(
          this.topicEventService.selectedTopic,
          this.clusterEventService.selectedCluster,
        )
        .subscribe(
          data => (this.data = data),
          err => {
            const notification = new Notification({
              type: INotificationType.error,
              message: err.error.message,
            });
            this.toasterConfig = notification.config;
            this.toasterService.popAsync(notification.toaster);
            console.error(err);
          },
        );
    }
  }

  expandCollapseAcls() {
    this.showAcls = !this.showAcls;
    return this.showAcls;
  }

  onCreate(event) {
    const newData = event.newData;

    if (!this.selectedTopic) {
      const notification = new Notification({
        type: INotificationType.error,
        message: 'Please select a topic from the topic switcher above',
      });
      this.toasterConfig = notification.config;
      this.toasterService.popAsync(notification.toaster);
      return;
    }

    if (this.checkEmptiness(newData)) {
      const notification = new Notification({
        type: INotificationType.error,
        message: 'All fields are required',
      });
      this.toasterConfig = notification.config;
      this.toasterService.popAsync(notification.toaster);
      return;
    }

    if (!this.checkPrincipal(newData.principal)) {
      return;
    }

    const acl = this.createAclBinding(newData);
    this.aclService
      .createAcl(this.clusterEventService.selectedCluster, acl)
      .subscribe(
        () => {
          this.queryAcls();
          event.confirm.resolve();
        },
        err => {
          const notification = new Notification({
            type: INotificationType.error,
            message: err.error.message,
          });
          this.toasterConfig = notification.config;
          this.toasterService.popAsync(notification.toaster);
          console.error(err);
        },
      );
  }

  createAclBinding(data) {
    const acl: AclBinding = {
      resource: {
        name: this.selectedTopic,
        resourceType: ResourceType.TOPIC,
      },
      entry: {
        data: {
          principal: data.principal,
          host: data.host,
          operation: data.operation,
          permissionType: data.permissionType,
        },
      },
    };
    return acl;
  }

  checkPrincipal(principal: string) {
    const splits: string[] = principal.split(':');
    if (splits.length !== 2) {
      const notification = new Notification({
        type: INotificationType.error,
        message: 'Principal malformed. Expected egroup:egroup-name',
      });
      this.toasterConfig = notification.config;
      this.toasterService.popAsync(notification.toaster);
      return false;
    }

    if (splits[0] !== 'egroup') {
      const notification = new Notification({
        type: INotificationType.error,
        message: 'The principal type accepted as of this moment is only egroup',
      });
      this.toasterConfig = notification.config;
      this.toasterService.popAsync(notification.toaster);
      return false;
    }

    return true;
  }

  checkEmptiness(data) {
    if (
      (data.principal === '',
      data.host === '',
      data.operation === '',
      data.permissionType === '')
    ) {
      return true;
    }
    return false;
  }

  onDelete(event) {
    const data = event.data;
    const acl = this.createAclBinding(data);

    if (
      window.confirm(
        'Are you sure you want to delete the acl with principal ->' +
          data.principal +
          ', host -> ' +
          data.host +
          ', operation -> ' +
          data.operation +
          ' and permission -> ' +
          data.permissionType +
          '?',
      )
    ) {
      this.aclService
        .deleteAcl(this.clusterEventService.selectedCluster, acl)
        .subscribe(
          () => {
            this.queryAcls();
          },
          err => {
            const notification = new Notification({
              type: INotificationType.error,
              message: err.error.message,
            });
            this.toasterConfig = notification.config;
            this.toasterService.popAsync(notification.toaster);
            console.log(err);
          },
        );
      return;
    }

    event.confirm.reject();
  }
}
