import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { KeyValueNoArrayPipe } from '../../@theme/pipes/kvNoArray.pipe';
import { TopicService } from '../../services/topic.service';
import { NileConfigEntry } from '../../models/nileconfigWrapper.model';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { TopicEventService } from '../../services/events/topicEvent.service';

@Component({
  selector: 'cern-topic-edit-modal',
  templateUrl: './topic-edit-modal.component.html',
  styleUrls: ['./topic-edit-modal.component.scss'],
})
export class TopicEditModalComponent implements OnInit {
  topicData: any = {};
  topicDataPiped: any = {};
  increasePartitions: boolean = false;
  opt: any;
  partitions: number;

  compressionOpts = ['producer', 'snappy', 'uncompressed', 'gzip', 'lz4'];

  constructor(
    private activeModal: NgbActiveModal,
    private kvPipe: KeyValueNoArrayPipe,
    private topicService: TopicService,
    private clusterEventService: ClusterEventService,
    private topicEventService: TopicEventService,
  ) {}

  ngOnInit() {
    this.topicDataPiped = this.kvPipe.transform(this.topicData.config);
  }

  onSubmit() {
    if (this.partitions) {
      this.requestPartitionsIncrement();
    }

    const toRetrieve: string[] = [
      'compression.type',
      'retention.ms',
      'min.insync.replicas',
    ];

    const entries: NileConfigEntry[] = [];
    toRetrieve.forEach(element => {
      if (this.topicDataPiped.hasOwnProperty(element)) {
        entries.push({
          name: element,
          value: this.topicDataPiped[element],
        });
      }
    });

    this.topicService
      .alterConfigTopic(
        this.topicData.name,
        this.clusterEventService.selectedCluster,
        {
          config: {
            entries: entries,
          },
        },
      )
      .subscribe(
        () => this.topicEventService.dispatchUpdate(this.topicData.name),
        err => console.error(err),
      );

    this.activeModal.close();
  }

  requestPartitionsIncrement() {
    this.topicService
      .increaseTopicPartitions(
        this.clusterEventService.selectedCluster,
        this.topicData.name,
        this.partitions,
      )
      .subscribe(
        () => this.topicEventService.dispatchUpdate(this.topicData.name),
        err => console.error(err),
      );
  }

  closeModal() {
    this.activeModal.close();
  }

  onPartitionsTick() {
    this.increasePartitions = !this.increasePartitions;
  }
}
