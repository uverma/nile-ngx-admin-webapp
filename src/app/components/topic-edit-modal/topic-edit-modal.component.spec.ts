import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicEditModalComponent } from './topic-edit-modal.component';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { KeyValueNoArrayPipe } from '../../@theme/pipes/kvNoArray.pipe';
import { TopicService } from '../../services/topic.service';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { TopicEventService } from '../../services/events/topicEvent.service';

describe('TopicEditModalComponent', () => {
  let component: TopicEditModalComponent;
  let fixture: ComponentFixture<TopicEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TopicEditModalComponent],
      providers: [
        NgbActiveModal,
        KeyValueNoArrayPipe,
        TopicService,
        HttpClient,
        HttpHandler,
        ClusterEventService,
        TopicEventService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
