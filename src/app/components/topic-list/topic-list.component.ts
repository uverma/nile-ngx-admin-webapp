import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TopicService } from '../../services/topic.service';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import Notification, {
  INotificationType,
} from '../notifications/notifications.component';
import { TopicEventService } from '../../services/events/topicEvent.service';

@Component({
  selector: 'cern-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.scss'],
})
export class TopicListComponent implements OnInit {
  data: any[];
  settings: {};
  showTopics = true;
  toasterConfig: ToasterConfig;

  @Output() onUserRowSelectedEvent: EventEmitter<any> = new EventEmitter();
  @Output() onPopulating: EventEmitter<any> = new EventEmitter();

  constructor(
    private topicService: TopicService,
    private clusterEventService: ClusterEventService,
    private toasterService: ToasterService,
    private topicEventService: TopicEventService,
  ) {
    this.settings = this.topicService.topicSettings();
    this.clusterEventService.events.subscribe(() => {
      this.queryTopics(() => {
        this.onPopulating.emit(this.data[0]);
      });
    });
    this.topicEventService.updateEvents.subscribe(() => {
      this.queryTopics(topicname => {
        const id = this.findIndex(topicname);
        this.onPopulating.emit(this.data[id !== -1 ? id : 0]);
      });
    });
  }

  findIndex(topicname): number {
    return this.data.findIndex(t => t.name === topicname);
  }

  ngOnInit() {
    this.queryTopics(() => {
      this.onPopulating.emit(this.data[0]);
    });
  }

  queryTopics(cb?) {
    if (
      this.clusterEventService.selectedCluster !== undefined &&
      this.clusterEventService.selectedCluster !== ''
    ) {
      this.topicService
        .getTopics(this.clusterEventService.selectedCluster)
        .subscribe(
          data => {
            this.data = data;
            if (cb) cb();
          },
          err => {
            const notification = new Notification({
              type: INotificationType.error,
              message: err.error.message,
            });
            this.toasterConfig = notification.config;
            this.toasterService.popAsync(notification.toaster);
            console.log(err);
          },
        );
    }
  }

  expandCollapseTopics() {
    this.showTopics = !this.showTopics;
    return this.showTopics;
  }

  onUserRowSelected(event) {
    this.onUserRowSelectedEvent.emit(event);
  }

  onCreate(event) {
    const newData = event.newData;
    const name = newData.name;
    const partitions = newData.partitions;
    const egroup = newData.egroup;
    const replicas = newData.replicas;

    if (!name || !partitions || !egroup || !replicas) {
      const notification = new Notification({
        type: INotificationType.error,
        message: 'All fields are required.',
      });
      this.toasterConfig = notification.config;
      this.toasterService.popAsync(notification.toaster);
      return;
    }

    this.topicService
      .createTopic(
        this.clusterEventService.selectedCluster,
        name,
        partitions,
        egroup,
        replicas,
      )
      .subscribe(
        () => {
          this.queryTopics(() => {
            this.onPopulating.emit(this.data[0]);
          });
          event.confirm.resolve();
        },
        err => {
          const notification = new Notification({
            type: INotificationType.error,
            message: err.error.message,
          });
          this.toasterConfig = notification.config;
          this.toasterService.popAsync(notification.toaster);
          console.log(err);
        },
      );
  }
  onDelete(event) {
    if (
      window.confirm(
        'Are you sure you want to delete the topic ' + event.data.name + '?',
      )
    ) {
      this.topicService
        .deleteTopic(this.clusterEventService.selectedCluster, event.data.name)
        .subscribe(
          () => {
            this.queryTopics(() => {
              this.onPopulating.emit(this.data[0]);
            });
          },
          err => {
            const notification = new Notification({
              type: INotificationType.error,
              message: err.error.message,
            });
            this.toasterConfig = notification.config;
            this.toasterService.popAsync(notification.toaster);
            console.log(err);
          },
        );
      return;
    }

    event.confirm.reject();
  }
}
