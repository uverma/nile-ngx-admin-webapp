import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicListComponent } from './topic-list.component';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { RouterTestingModule } from '@angular/router/testing';
import { TopicService } from '../../services/topic.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { FlatPropsTopicPipe } from '../../@theme/pipes/flatProps.pipe';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { TopicEventService } from '../../services/events/topicEvent.service';

describe('TopicListComponent', () => {
  let component: TopicListComponent;
  let fixture: ComponentFixture<TopicListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ThemeModule,
        Ng2SmartTableModule,
        RouterTestingModule,
        ToasterModule,
      ],
      declarations: [TopicListComponent, FlatPropsTopicPipe],
      providers: [
        TopicService,
        HttpClient,
        HttpHandler,
        ClusterEventService,
        ToasterService,
        TopicEventService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
