import { ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

export enum INotificationType {
  default = 'default',
  info = 'info',
  success = 'success',
  warning = 'warning',
  error = 'error',
}

export interface INotification {
  type: INotificationType;
  message: string;
}

export default class Notification {
  config: ToasterConfig;
  toaster: Toast;

  constructor(override: INotification) {
    console.log(override);
    this.config = new ToasterConfig({
      positionClass: 'toast-top-center',
      timeout: 3000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: true,
      animation: 'slideDown',
      limit: 3,
    });
    this.toaster = {
      type: override.type.toString(),
      body: override.message,
      timeout: 3000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
  }
}
