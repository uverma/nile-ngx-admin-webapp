import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { OverviewComponent } from './overview.component';
import { ClusterService } from '../../services/cluster.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TopicService } from '../../services/topic.service';
import { ClusterStatsComponent } from '../../components/cluster-stats/cluster-stats.component';
import { AclService } from '../../services/acl.service';
import { FormsModule } from '@angular/forms';
import { ClusterInfoComponent } from '../../components/cluster-info/cluster-info.component';
import { ObjectToObjectArrayPipe } from '../../@theme/pipes/objToArrObjs.pipe';
import { ToasterModule } from 'angular2-toaster';

@NgModule({
  imports: [ThemeModule, Ng2SmartTableModule, FormsModule, ToasterModule],
  declarations: [
    OverviewComponent,
    ClusterStatsComponent,
    ClusterInfoComponent,
    ObjectToObjectArrayPipe,
  ],
  providers: [ClusterService, TopicService, AclService],
})
export class OverviewModule {}
