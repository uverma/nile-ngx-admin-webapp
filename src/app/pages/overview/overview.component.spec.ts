import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewComponent } from './overview.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TopicListComponent } from '../../components/topic-list/topic-list.component';
import { AclListComponent } from '../../components/acl-list/acl-list.component';
import { ClusterStatsComponent } from '../../components/cluster-stats/cluster-stats.component';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { ClusterInfoComponent } from '../../components/cluster-info/cluster-info.component';
import { ObjectToObjectArrayPipe } from '../../@theme/pipes/objToArrObjs.pipe';
import {
  FlatPropsTopicPipe,
  FlatPropsAclPipe,
} from '../../@theme/pipes/flatProps.pipe';
import { ClusterService } from '../../services/cluster.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ToasterModule, ToasterService } from 'angular2-toaster';

describe('OverviewComponent', () => {
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ThemeModule,
        Ng2SmartTableModule,
        ToasterModule,
      ],
      declarations: [
        OverviewComponent,
        TopicListComponent,
        AclListComponent,
        ClusterStatsComponent,
        ClusterInfoComponent,
        ObjectToObjectArrayPipe,
        FlatPropsTopicPipe,
        FlatPropsAclPipe,
      ],
      providers: [
        ClusterEventService,
        ClusterService,
        HttpClient,
        HttpHandler,
        ToasterService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
