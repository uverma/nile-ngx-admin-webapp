import { Component, OnInit } from '@angular/core';
import { ClusterEventService } from '../../services/events/clusterEvent.service';

@Component({
  selector: 'cern-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.scss'],
})
export class TopicsComponent implements OnInit {
  topicData: any = {};

  constructor(private clusterEventService: ClusterEventService) {
    this.clusterEventService.events.subscribe(() => {
      this.topicData = null;
    });
  }
  ngOnInit() {}

  onUserRowSelected(event) {
    this.topicData = event.data;
    console.log(event.data);
  }

  onInitPop(data) {
    if (data !== undefined) {
      this.topicData = data;
    }
  }
}
