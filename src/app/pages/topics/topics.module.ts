import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { TopicsComponent } from './topics.component';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { TopicListComponent } from '../../components/topic-list/topic-list.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TopicInfoComponent } from '../../components/topic-info/topic-info.component';
import { TopicEditModalComponent } from '../../components/topic-edit-modal/topic-edit-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { KeyValuePipe } from '../../@theme/pipes/kv.pipe';
import { KeyValueNoArrayPipe } from '../../@theme/pipes/kvNoArray.pipe';
import { FlatPropsTopicPipe } from '../../@theme/pipes/flatProps.pipe';
import { ToasterModule } from 'angular2-toaster';

@NgModule({
  imports: [ThemeModule, Ng2SmartTableModule, ToasterModule],
  declarations: [
    TopicsComponent,
    TopicListComponent,
    TopicInfoComponent,
    TopicEditModalComponent,
    KeyValuePipe,
    KeyValueNoArrayPipe,
    FlatPropsTopicPipe,
  ],
  providers: [ClusterEventService, NgbModal, KeyValuePipe, KeyValueNoArrayPipe],
  entryComponents: [TopicEditModalComponent],
})
export class TopicsModule {}
