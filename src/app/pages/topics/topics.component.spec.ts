import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicsComponent } from './topics.component';
import { ThemeModule } from '../../@theme/theme.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TopicListComponent } from '../../components/topic-list/topic-list.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TopicService } from '../../services/topic.service';
import { TopicInfoComponent } from '../../components/topic-info/topic-info.component';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { KeyValuePipe } from '../../@theme/pipes/kv.pipe';
import { FlatPropsTopicPipe } from '../../@theme/pipes/flatProps.pipe';
import { ToasterService, ToasterModule } from 'angular2-toaster';
import { TopicEventService } from '../../services/events/topicEvent.service';

describe('TopicsComponent', () => {
  let component: TopicsComponent;
  let fixture: ComponentFixture<TopicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ThemeModule,
        RouterTestingModule,
        Ng2SmartTableModule,
        ToasterModule,
      ],
      declarations: [
        TopicsComponent,
        TopicListComponent,
        TopicInfoComponent,
        KeyValuePipe,
        FlatPropsTopicPipe,
      ],
      providers: [
        HttpClient,
        HttpHandler,
        ClusterEventService,
        TopicService,
        NgbModalStack,
        ToasterService,
        TopicEventService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
