import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { OverviewModule } from './overview/overview.module';
import { TopicsModule } from './topics/topics.module';
import { AclsModule } from './acls/acls.module';
import { ComingSoonComponent } from '../components/coming-soon/coming-soon.component';
import { RequestClusterComponent } from './request-cluster/request-cluster.component';
import { GroupAclComponent } from './group-acl/group-acl.component';
import { ToasterModule } from 'angular2-toaster';

const PAGES_COMPONENTS = [
  PagesComponent,
  RequestClusterComponent,
  GroupAclComponent,
  ComingSoonComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    OverviewModule,
    TopicsModule,
    AclsModule,
    ToasterModule,
  ],
  declarations: [...PAGES_COMPONENTS],
})
export class PagesModule {}
