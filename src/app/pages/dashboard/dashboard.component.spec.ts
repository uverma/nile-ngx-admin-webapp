import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { ThemeModule } from '../../@theme/theme.module';
import { ServiceMetricsComponent } from '../../components/service-metrics/service-metrics.component';
import { SafePipe } from '../../@theme/pipes/safe.pipe';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { ToasterService, ToasterModule } from 'angular2-toaster';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThemeModule, ToasterModule],
      declarations: [DashboardComponent, ServiceMetricsComponent, SafePipe],
      providers: [ClusterEventService, ToasterService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
