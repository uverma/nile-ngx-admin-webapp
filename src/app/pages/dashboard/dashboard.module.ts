import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { ServiceMetricsComponent } from '../../components/service-metrics/service-metrics.component';
import { SafePipe } from '../../@theme/pipes/safe.pipe';

@NgModule({
  imports: [ThemeModule],
  declarations: [DashboardComponent, ServiceMetricsComponent, SafePipe],
})
export class DashboardModule {}
