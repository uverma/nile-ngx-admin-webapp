import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AclsComponent } from './acls.component';
import { ThemeModule } from '../../@theme/theme.module';
import { AclListComponent } from '../../components/acl-list/acl-list.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { RouterTestingModule } from '@angular/router/testing';
import { AclService } from '../../services/acl.service';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { TopicSwitcherComponent } from '../../components/topic-switcher/topic-switcher.component';
import { TopicEventService } from '../../services/events/topicEvent.service';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TopicService } from '../../services/topic.service';
import { FlatPropsAclPipe } from '../../@theme/pipes/flatProps.pipe';
import { ToasterService, ToasterModule } from 'angular2-toaster';

describe('AclsComponent', () => {
  let component: AclsComponent;
  let fixture: ComponentFixture<AclsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ThemeModule,
        Ng2SmartTableModule,
        RouterTestingModule,
        FormsModule,
        NgSelectModule,
        ToasterModule,
      ],
      declarations: [
        AclsComponent,
        AclListComponent,
        TopicSwitcherComponent,
        FlatPropsAclPipe,
      ],
      providers: [
        AclService,
        HttpClient,
        HttpHandler,
        ClusterEventService,
        TopicEventService,
        TopicService,
        ToasterService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AclsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
