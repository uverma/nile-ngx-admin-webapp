import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ClusterEventService } from '../../services/events/clusterEvent.service';
import { AclsComponent } from './acls.component';
import { AclListComponent } from '../../components/acl-list/acl-list.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TopicSwitcherComponent } from '../../components/topic-switcher/topic-switcher.component';
import { ClusterService } from '../../services/cluster.service';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TopicEventService } from '../../services/events/topicEvent.service';
import { FlatPropsAclPipe } from '../../@theme/pipes/flatProps.pipe';
import { ToasterModule } from 'angular2-toaster';

@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
    FormsModule,
    NgSelectModule,
    ToasterModule,
  ],
  declarations: [
    AclsComponent,
    AclListComponent,
    TopicSwitcherComponent,
    FlatPropsAclPipe,
  ],
  providers: [ClusterEventService, ClusterService, TopicEventService],
})
export class AclsModule {}
