import { Component, OnInit } from '@angular/core';
import { TopicEventService } from '../../services/events/topicEvent.service';

@Component({
  selector: 'cern-acls',
  templateUrl: './acls.component.html',
  styleUrls: ['./acls.component.scss'],
})
export class AclsComponent implements OnInit {
  selectedTopic: string;

  constructor(private topicEventService: TopicEventService) {
    this.topicEventService.events.subscribe(() => {
      this.selectedTopic = this.topicEventService.selectedTopic;
    });
  }

  ngOnInit() {}
}
