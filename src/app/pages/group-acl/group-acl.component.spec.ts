import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GroupAclComponent } from './group-acl.component';
import { ThemeModule } from '../../@theme/theme.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ComingSoonComponent } from '../../components/coming-soon/coming-soon.component';

describe('GroupConfigComponent', () => {
  let component: GroupAclComponent;
  let fixture: ComponentFixture<GroupAclComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThemeModule, RouterTestingModule],
      declarations: [GroupAclComponent, ComingSoonComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupAclComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
