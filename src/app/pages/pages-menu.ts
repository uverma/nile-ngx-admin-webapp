import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Overview',
    icon: 'nb-grid-a',
    link: '/pages/overview',
    home: false,
  },
  {
    title: 'Topic Configuration',
    icon: 'fa fa-wrench',
    link: '/pages/topics',
    home: false,
  },
  {
    title: 'Topic Acls',
    icon: 'fa fa-user-secret',
    link: '/pages/acls',
    home: false,
  },
  {
    title: 'Group Acls',
    icon: 'fa fa-user-secret',
    link: '/pages/groups',
    home: false,
  },
  {
    title: 'Request Cluster',
    icon: 'fa fa-wpforms',
    link: '/pages/cluster-request',
    home: false,
  },
];
