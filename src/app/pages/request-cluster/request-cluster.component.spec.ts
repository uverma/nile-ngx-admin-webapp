import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestClusterComponent } from './request-cluster.component';
import { ThemeModule } from '../../@theme/theme.module';
import { ComingSoonComponent } from '../../components/coming-soon/coming-soon.component';

describe('RequestClusterComponent', () => {
  let component: RequestClusterComponent;
  let fixture: ComponentFixture<RequestClusterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThemeModule],
      declarations: [RequestClusterComponent, ComingSoonComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
