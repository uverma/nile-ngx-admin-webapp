import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RequestClusterComponent } from './request-cluster/request-cluster.component';
import { TopicsComponent } from './topics/topics.component';
import { AclsComponent } from './acls/acls.component';
import { OverviewComponent } from './overview/overview.component';
import { GroupAclComponent } from './group-acl/group-acl.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'overview',
        component: OverviewComponent,
      },
      {
        path: 'topics',
        component: TopicsComponent,
      },
      {
        path: 'acls',
        component: AclsComponent,
      },
      {
        path: 'cluster-request',
        component: RequestClusterComponent,
      },
      {
        path: 'groups',
        component: GroupAclComponent,
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: '**',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
