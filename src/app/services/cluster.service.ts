import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import 'rxjs/add/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ClusterService {
  clusternameHeader = 'ClusterHostname';

  constructor(private http: HttpClient) {}

  getClusters(): Observable<any> {
    if (environment.production === false) {
      // return this.mockClusters();
      return this.http.get('https://kapiato.cern.ch/dal/cluster', {
        headers: new HttpHeaders().set('Authorization', environment.token),
      });
    }
    return this.http.get('api/dal/cluster');
  }

  getClusterInfo(clustername: string): Observable<any> {
    if (environment.production === false) {
      // return this.mockClusterInfo(clustername);
      return this.http.get('https://kapiato.cern.ch/cluster', {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername),
      });
    }
    return this.http.get('api/cluster', {
      headers: new HttpHeaders().set(this.clusternameHeader, clustername),
    });
  }

  mockClusterInfo(clustername): Observable<any> {
    if (clustername === 'cern-0') {
      return Observable.of({
        clusterId: 0,
        controller: {
          id: 1,
          idString: '1',
          host: 'cern-service-dev-0-1.cern.ch',
          port: 9092,
          rack: 'cern-geneva-a',
        },
        nodes: [
          {
            id: 1,
            idString: '1',
            host: 'cern-service-dev-0-1.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
          {
            id: 2,
            idString: '2',
            host: 'cern-service-dev-0-2.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
          {
            id: 3,
            idString: '3',
            host: 'cern-service-dev-0-3.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
        ],
      });
    } else if (clustername === 'cern-1') {
      return Observable.of({
        clusterId: 1,
        controller: {
          id: 1,
          idString: '1',
          host: 'cern-service-dev-1-1.cern.ch',
          port: 9092,
          rack: 'cern-geneva-a',
        },
        nodes: [
          {
            id: 1,
            idString: '1',
            host: 'cern-service-dev-1-1.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
          {
            id: 2,
            idString: '2',
            host: 'cern-service-dev-1-2.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
        ],
      });
    } else {
      return Observable.of({
        clusterId: 2,
        controller: {
          id: 1,
          idString: '1',
          host: 'cern-service-dev-2-1.cern.ch',
          port: 9092,
          rack: 'cern-geneva-a',
        },
        nodes: [
          {
            id: 1,
            idString: '1',
            host: 'cern-service-dev-2-1.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
          {
            id: 2,
            idString: '2',
            host: 'cern-service-dev-2-2.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
          {
            id: 3,
            idString: '3',
            host: 'cern-service-dev-2-3.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
          {
            id: 4,
            idString: '4',
            host: 'cern-service-dev-2-4.cern.ch',
            port: 9092,
            rack: 'cern-geneva-a',
          },
        ],
      });
    }
  }

  mockClusters(): Observable<any> {
    return Observable.of([
      {
        cluster_id: 0,
        name: 'cern-0',
        owner: 'cern',
        e_group: 'it-db-nile-admins',
        creation_date: '2018-04-12',
        expiry_date: '2018-04-12',
        project: 'nile',
        description: 'test-nile',
        version: '1.0.0',
      },
      {
        cluster_id: 1,
        name: 'cern-1',
        owner: 'cern1',
        e_group: 'it-db-nile-admins',
        creation_date: '2018-04-12',
        expiry_date: '2018-04-12',
        project: 'nile1',
        description: 'test-nile-1',
        version: '1.0.0',
      },
      {
        cluster_id: 2,
        name: 'cern-2',
        owner: 'cern2',
        e_group: 'it-db-nile-admins',
        creation_date: '2018-04-12',
        expiry_date: '2018-04-12',
        project: 'nile2',
        description: 'test-nile-2',
        version: '1.0.0',
      },
    ]);
  }

  clusterSettings() {
    return {
      hideSubHeader: false,
      actions: { add: false, edit: false, delete: false },
      pager: { display: true, perPage: 20 },
      columns: {
        name: {
          title: 'Name',
        },
        version: {
          title: 'Version',
        },
        owner: {
          title: 'Owner',
        },
        hostnames: {
          title: 'Hostnames',
        },
      },
    };
  }
}
