import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/observable/of';
import {
  NileConfigResourceConfigWrapper,
  ConfigType,
} from '../models/nileconfigWrapper.model';

@Injectable()
export class TopicService {
  clusternameHeader = 'ClusterHostname';

  constructor(private http: HttpClient) {}

  getTopics(clustername: string): Observable<any> {
    if (environment.production === false) {
      // return this.mockGet(clustername);
      return this.http.get('https://kapiato.cern.ch/topic/configs', {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername),
      });
    }

    // need to define api endpoints
    return this.http.get('/api/topic/configs', {
      headers: new HttpHeaders().set('ClusterHostname', clustername),
    });
  }

  createTopic(
    clustername: string,
    topicname: string,
    partitions: number,
    egroup: string,
    replicas: number,
  ) {
    const obj = {
      name: topicname,
      numPartitions: partitions,
      replicationFactor: replicas,
    };

    obj.replicationFactor = 0;

    if (environment.production === false) {
      return this.http.post('https://kapiato.cern.ch/topic', obj, {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername)
          .set('egroup', egroup),
      });
    }

    return this.http.post('/api/topic', obj, {
      headers: new HttpHeaders()
        .set(this.clusternameHeader, clustername)
        .set('egroup', egroup),
    });
  }

  alterConfigTopic(
    topicname: string,
    clustername: string,
    configs: NileConfigResourceConfigWrapper,
  ) {
    configs.configResource = {
      name: topicname,
      type: ConfigType.TOPIC,
    };
    if (environment.production === false) {
      return this.http.put('https://kapiato.cern.ch/config', configs, {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername),
      });
    }

    return this.http.put('/api/config', configs, {
      headers: new HttpHeaders().set(this.clusternameHeader, clustername),
    });
  }

  deleteTopic(clustername: string, topicname: string) {
    if (environment.production === false) {
      return this.http.delete('https://kapiato.cern.ch/topic/' + topicname, {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername),
      });
    }

    return this.http.delete('/api/topic/' + topicname, {
      headers: new HttpHeaders().set(this.clusternameHeader, clustername),
    });
  }

  increaseTopicPartitions(
    clustername: string,
    topicname: string,
    partitions: number,
  ) {
    const obj = {
      topic: topicname,
      totalCount: partitions,
    };

    if (environment.production === false) {
      return this.http.post('https://kapiato.cern.ch/partition', obj, {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername),
      });
    }

    return this.http.post('/api/partition', obj, {
      headers: new HttpHeaders().set(this.clusternameHeader, clustername),
    });
  }

  mockGet(clustername): Observable<any> {
    if (clustername === 'cern-0') {
      return Observable.of([
        {
          topic: {
            name: 'cern-0-topic-i',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '130000',
            },
            {
              name: 'min.insync.replicas',
              value: '10',
            },
          ],
        },
        {
          topic: {
            name: 'cern-0-topic-ii',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '10000',
            },
            {
              name: 'min.insync.replicas',
              value: '6',
            },
          ],
        },
        {
          topic: {
            name: 'cern-0-topic-iii',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '512923',
            },
            {
              name: 'min.insync.replicas',
              value: '1',
            },
          ],
        },
      ]);
    } else if (clustername === 'cern-1') {
      return Observable.of([
        {
          topic: {
            name: 'cern-1-topic-i',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '1051234',
            },
            {
              name: 'min.insync.replicas',
              value: '112',
            },
          ],
        },
        {
          topic: {
            name: 'cern-1-topic-ii',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '457453',
            },
            {
              name: 'min.insync.replicas',
              value: '74',
            },
          ],
        },
        {
          topic: {
            name: 'cern-1-topic-iii',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '10',
            },
            {
              name: 'min.insync.replicas',
              value: '1',
            },
          ],
        },
      ]);
    } else {
      return Observable.of([
        {
          topic: {
            name: 'cern-2-topic-i',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '4526',
            },
            {
              name: 'min.insync.replicas',
              value: '26',
            },
          ],
        },
        {
          topic: {
            name: 'cern-2-topic-ii',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '2457',
            },
            {
              name: 'min.insync.replicas',
              value: '27',
            },
          ],
        },
        {
          topic: {
            name: 'cern-2-topic-iii',
            partitions: [
              {
                partition: 0,
                leader: {
                  id: 0,
                  idString: '0',
                  host: '',
                  rack: '',
                },
                replicas: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
                isr: [
                  {
                    id: 0,
                    idString: '0',
                    host: '',
                    rack: '',
                  },
                ],
              },
            ],
          },
          config: [
            {
              name: 'compression.type',
              value: 'Snappy',
            },
            {
              name: 'retention.ms',
              value: '1435260',
            },
            {
              name: 'min.insync.replicas',
              value: '152',
            },
          ],
        },
      ]);
    }
  }

  topicSettings(): object {
    return {
      hideSubHeader: false,
      actions: { position: 'right', add: true, edit: false, delete: true },
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      pager: { display: true, perPage: 20 },
      columns: {
        name: {
          title: 'Name',
        },
        partitions: {
          title: '# Partitions',
        },
        replicas: {
          title: 'Replication Factor',
        },
        egroup: {
          title: 'E-Group',
        },
      },
    };
  }
}
