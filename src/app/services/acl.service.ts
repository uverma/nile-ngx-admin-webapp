import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import 'rxjs/add/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AclBinding } from '../models/acl.model';

@Injectable()
export class AclService {
  clusternameHeader = 'ClusterHostname';

  constructor(private http: HttpClient) {}

  getAcls(topicname: string, clustername: string): Observable<any[]> {
    if (environment.production === false) {
      // return this.mockAcls(topicname);
      return this.http.get<any[]>('https://kapiato.cern.ch/acl/' + topicname, {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername),
      });
    }
    return this.http.get<any[]>('api/acl/' + topicname, {
      headers: new HttpHeaders().set(this.clusternameHeader, clustername),
    });
  }

  createAcl(clustername: string, acl: AclBinding) {
    if (environment.production === false) {
      return this.http.post('https://kapiato.cern.ch/acl', acl, {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername),
      });
    }

    return this.http.post('api/acl', acl, {
      headers: new HttpHeaders().set(this.clusternameHeader, clustername),
    });
  }

  deleteAcl(clustername: string, acl: AclBinding) {
    if (environment.production === false) {
      return this.http.post('https://kapiato.cern.ch/acl/del', acl, {
        headers: new HttpHeaders()
          .set('Authorization', environment.token)
          .set(this.clusternameHeader, clustername),
      });
    }

    return this.http.post('api/acl/del', acl, {
      headers: new HttpHeaders().set(this.clusternameHeader, clustername),
    });
  }

  mockAcls(topicname: string): Observable<any[]> {
    const i = Math.floor(Math.random() * 10 + 1);
    if (i >= 1 && i < 3) {
      return Observable.of([
        {
          resource: {
            resourceType: '',
            name: 'WTV',
          },
          entry: {
            data: {
              principal: 'egroup:it-db-nile-admins',
              host: '*',
              operation: 'ALL',
              permission: 'ALLOW',
            },
          },
        },
        {
          resource: {
            resourceType: '',
            name: 'WTV',
          },
          entry: {
            data: {
              principal: 'egroup:it-db-nile-admins',
              host: '*',
              operation: 'ALL',
              permission: 'ALLOW',
            },
          },
        },
      ]);
    } else if (i >= 3 && i <= 6) {
      return Observable.of([
        {
          resource: {
            resourceType: '',
            name: 'WTV',
          },
          entry: {
            data: {
              principal: 'egroup:it-db-nile-admins',
              host: '*',
              operation: 'ALL',
              permission: 'ALLOW',
            },
          },
        },
      ]);
    } else {
      return Observable.of([
        {
          resource: {
            resourceType: '',
            name: 'WTV',
          },
          entry: {
            data: {
              principal: 'egroup:it-db-nile-admins',
              host: '*',
              operation: 'ALL',
              permission: 'ALLOW',
            },
          },
        },
        {
          resource: {
            resourceType: '',
            name: 'WTV',
          },
          entry: {
            data: {
              principal: 'egroup:it-db-nile-admins',
              host: '*',
              operation: 'ALL',
              permission: 'ALLOW',
            },
          },
        },
        {
          resource: {
            resourceType: '',
            name: 'WTV',
          },
          entry: {
            data: {
              principal: 'egroup:it-db-nile-admins',
              host: '*',
              operation: 'ALL',
              permission: 'ALLOW',
            },
          },
        },
      ]);
    }
  }

  aclSettings() {
    return {
      hideSubHeader: false,
      actions: { position: 'right', add: true, edit: false, delete: true },
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      pager: { display: true, perPage: 20 },
      columns: {
        principal: {
          title: 'Principal',
        },
        host: {
          title: 'Host',
        },
        operation: {
          title: 'Operation',
          editor: {
            type: 'list',
            config: {
              list: this.getOperationOptions(),
            },
          },
        },
        permissionType: {
          title: 'Permission',
          editor: {
            type: 'list',
            config: {
              list: this.getPermissionOptions(),
            },
          },
        },
      },
    };
  }

  getOperationOptions() {
    return [
      { value: 'ALL', title: 'All' },
      { value: 'READ', title: 'Read' },
      { value: 'WRITE', title: 'Write' },
      { value: 'DELETE', title: 'Delete' },
      { value: 'DESCRIBE', title: 'Describe' },
      { value: 'DESCRIBE_CONFIGS', title: 'Describe Configs' },
      { value: 'ALTER_CONFIGS', title: 'Alter Configs' },
    ];
  }

  getPermissionOptions() {
    return [
      { value: 'ALLOW', title: 'Allow' },
      { value: 'DENY', title: 'Deny' },
    ];
  }
}
