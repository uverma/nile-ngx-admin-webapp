import { Subject } from 'rxjs';

export class ClusterEventService {
  public selectedCluster = localStorage.getItem('selectedCluster')
    ? localStorage.getItem('selectedCluster')
    : '';

  public selectedClusterData;
  protected _eventSubject = new Subject();
  public events = this._eventSubject.asObservable();

  dispatchEvent(cluster) {
    if (cluster !== undefined) {
      localStorage.setItem('selectedCluster', cluster.name);
      this.selectedCluster = cluster.name;
      this.selectedClusterData = cluster;
      console.log(this.selectedClusterData);
      this._eventSubject.next();
    }
  }
}
