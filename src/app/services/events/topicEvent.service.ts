import { Subject } from 'rxjs';

export class TopicEventService {
  public selectedTopic = '';

  protected _eventSubject = new Subject();
  public events = this._eventSubject.asObservable();

  protected _updateSubject = new Subject();
  public updateEvents = this._updateSubject.asObservable();

  dispatchEvent(topic) {
    this.selectedTopic = topic;
    this._eventSubject.next();
  }

  dispatchUpdate(topicname) {
    this._updateSubject.next(topicname);
  }
}
