import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterSwitcherComponent } from './cluster-switcher.component';
import { ThemeModule } from '../../theme.module';
import { ClusterService } from '../../../services/cluster.service';
import { ClusterEventService } from '../../../services/events/clusterEvent.service';
import { HttpHandler, HttpClient } from '@angular/common/http';
import { ToasterModule, ToasterService } from 'angular2-toaster';

describe('ClusterSwitcherComponent', () => {
  let component: ClusterSwitcherComponent;
  let fixture: ComponentFixture<ClusterSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThemeModule, ToasterModule],
      declarations: [],
      providers: [
        ClusterService,
        ClusterEventService,
        HttpClient,
        HttpHandler,
        ToasterService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
