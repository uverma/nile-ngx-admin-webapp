import { Component } from '@angular/core';
import { ClusterService } from '../../../services/cluster.service';
import { ClusterEventService } from '../../../services/events/clusterEvent.service';

@Component({
  selector: 'cern-cluster-switcher',
  templateUrl: './cluster-switcher.component.html',
  styleUrls: ['./cluster-switcher.component.scss'],
})
export class ClusterSwitcherComponent {
  clusters: any[];
  cluster: any;

  constructor(
    private clusterService: ClusterService,
    private clusterEventService: ClusterEventService,
  ) {
    this.clusterService.getClusters().subscribe(
      data => (this.clusters = data),
      err => console.error(err),
      () => {
        this.cluster = this.clusterEventService.selectedCluster;
        this.onClusterChange();
      },
    );
  }

  onClusterChange() {
    this.clusterEventService.dispatchEvent(this.findByName(this.cluster));
  }

  findByName(clusterName) {
    return this.clusters.find(p => p.name === clusterName);
  }
}
