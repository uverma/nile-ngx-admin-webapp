import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'kv' })
export class KeyValuePipe implements PipeTransform {
  transform(value, args?: string[]): any {
    const keys = [];
    for (const key in value) {
      if (value.hasOwnProperty(key)) {
        const element = value[key];
        const tKeys = Object.keys(element);
        const kk = element[tKeys[0]];
        const vv = element[tKeys[1]];

        keys.push({ key: kk, value: vv });
      }
    }
    return keys;
  }
}
