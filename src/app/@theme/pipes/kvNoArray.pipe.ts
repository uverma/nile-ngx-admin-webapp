import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'kvna' })
export class KeyValueNoArrayPipe implements PipeTransform {
  transform(value, args?: string[]): any {
    const obj = {};
    for (const key in value) {
      if (value.hasOwnProperty(key)) {
        const element = value[key];
        const tKeys = Object.keys(element);
        const kk = element[tKeys[0]];
        const vv = element[tKeys[1]];
        obj[kk] = vv;
      }
    }
    return obj;
  }
}
