import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'flatPropsTopic' })
export class FlatPropsTopicPipe implements PipeTransform {
  transform(value, args?: string[]): any {
    const objs = [];
    for (const topic in value) {
      if (value.hasOwnProperty(topic)) {
        const newObj = value[topic];
        const name = newObj.topic.name;
        const partitions = newObj.topic.partitions;
        if (name && partitions) {
          newObj['name'] = name;
          newObj['partitions'] = partitions.length;

          let i = 0;
          for (const part in partitions) {
            if (partitions.hasOwnProperty(part)) {
              i = partitions[part].replicas.length;
            }
          }
          newObj['replicas'] = i;
        }
        objs.push(newObj);
      }
    }
    return objs;
  }
}

@Pipe({ name: 'flatPropsAcl' })
export class FlatPropsAclPipe implements PipeTransform {
  transform(value, args?: string[]): any {
    const objs = [];
    for (const acl in value) {
      if (value.hasOwnProperty(acl)) {
        const newObj = value[acl];
        const entry = newObj.entry.data;

        for (const e in entry) {
          if (entry.hasOwnProperty(e)) {
            const val = entry[e];
            newObj[e] = val;
          }
        }
        objs.push(newObj);
      }
    }
    return objs;
  }
}
