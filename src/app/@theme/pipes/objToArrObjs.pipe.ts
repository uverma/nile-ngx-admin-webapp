import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'arrObjs' })
export class ObjectToObjectArrayPipe implements PipeTransform {
  transform(value, args?: string[]): any {
    const props = [];
    for (const key in value) {
      if (value.hasOwnProperty(key)) {
        props.push({ key: key, value: value[key] });
      }
    }
    return props;
  }
}
