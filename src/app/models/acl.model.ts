export enum ResourceType {
  TOPIC = 'TOPIC',
}

export enum Operation {
  ALL = 'ALL',
  READ = 'READ',
  WRITE = 'WRITE',
  DELETE = 'DELETE',
  DESCRIBE = 'DESCRIBE',
  DESCRIBE_CONFIGS = 'DESCRIBE_CONFIGS',
  ALTER_CONFIGS = 'ALTER_CONFIGS',
}

export enum Permission {
  ALLOW = 'ALLOW',
  DENY = 'DENY',
}

export interface AclBinding {
  resource: {
    resourceType: ResourceType;
    name: string;
  };
  entry: {
    data: {
      principal: string;
      host: string;
      operation: Operation;
      permissionType: Permission;
    };
  };
}
