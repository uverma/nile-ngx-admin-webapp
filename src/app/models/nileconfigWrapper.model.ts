export interface NileConfigResourceConfigWrapper {
  config: NileConfig;
  configResource?: NileConfigResource;
}

export interface NileConfig {
  entries: NileConfigEntry[];
}

export interface NileConfigEntry {
  name: string;
  value: string;
}

export interface NileConfigResource {
  name: string;
  type: ConfigType;
}

export enum ConfigType {
  BROKER = 'BROKER',
  TOPIC = 'TOPIC',
  UNKNOWN = 'UNKNOWN',
}
