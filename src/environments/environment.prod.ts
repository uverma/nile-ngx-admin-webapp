/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  token:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im5iYXJyb3MiLCJncm91cHMiOlsiQWxsIEV4Y2hhbmdlIF' +
    'Blb3BsZSIsIlVzZXJzIGJ5IEhvbWUgQ0VSTkhPTUVOIiwiVXNlcnMgYnkgTGV0dGVyIE4iLCJOSUNFIEVuZm9yY2UgUGFzc3dvcmQ' +
    'tcHJvdGVjdGVkIFNjcmVlbnNhdmVyIiwiYWlzLXVzZXJzIiwidXNlcnMtYXQtY2VybiIsIml0LWRlcC1mdWxsLWR5bmFtaWMiLCJpdC' +
    '1kZXAtZnVsbCIsIml0LWRiLW5pbGUtYWRtaW5zIiwiaXQtZGItbmlsZS11c2VycyJdLCJpYXQiOjE1MjUyNTY2NjZ9.XN11Pae45QFQq' +
    'DeMYwbrX0wkXQ22ScUiAajc-9FZM40',
};
